<?php
session_start();
include "gestionBD.inc.php";

if(isset($_POST["connect"])){
    if(!empty($_POST["email"]) && !empty($_POST["mdp"])){
        $email = htmlspecialchars($_POST["email"]);
        $mdp = htmlspecialchars($_POST["mdp"]);

        $pdo = connexionBd();
        $sql = "SELECT * FROM man_client WHERE email = :email";
        $select = $pdo->prepare($sql);
        $select->bindValue(":email", $email);
        $select->execute();
        $result = $select->fetch(PDO::FETCH_OBJ);

        if(password_verify($mdp, $result->mot_de_passe)){
            $_SESSION["login"] = $result;
            header("Location: index.php");
        }
        else {
            $messageError = "Email ou mot de passe incorrect";
        }

    }
}


?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/login.css">
    <title>Login</title>
</head>
<body>
    <?php if(isset($messageError)) : ?>
    <p><?= $messageError ?></p>
    <?php endif; ?>
    <form action="login.php" method="post">
        <label for="email">Email :</label>
        <input type="email" name="email" id="email" class="validate"/>

        <label for="mdp">Mot de passe :</label>
        <input type="password" name="mdp" id="mdp" class="validate"/>

        <input type="submit" name="connect" class="btn" value="Se connecter"/>
    </form>
</body>
</html>
