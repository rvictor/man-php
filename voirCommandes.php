<?php
session_start();
include "gestionBD.inc.php";

if(isset($_SESSION)){
    if(empty($_SESSION["login"])){
        header("Location: login.php");
    }

    $pdo = connexionBd();
    $id_client = $_SESSION["login"]->id;
    $sql = "SELECT * FROM man_commande WHERE id_client = :id_client ORDER BY date_commande DESC";
    $select = $pdo->prepare($sql);
    $select->bindValue(":id_client", $id_client);
    $select->execute();
    $result = $select->fetchAll(PDO::FETCH_OBJ);
    $dateActuelle = time();
}

?>


<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/table.css">
    <title>Mes commandes</title>
</head>
<body>
    <h1>Mes commandes</h1>
    <table>
        <thead>
            <tr>
                <th>Quantité</th>
                <th>Modèle</th>
                <th>Date</th>
                <th>Total</th>
                <th>Annulation</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($result as $commande) : ?>
            <tr>
                <td><?= $commande->quantite ?></td>
                <td><?= $commande->type?></td>
                <td><?= $commande->date_commande ?></td>
                <td><?= $commande->quantite * 15 ?></td>
                <?php if(((strtotime($commande->date_commande) - $dateActuelle) /3600) < 48) : ?>
                <td><a href="annulerCommande.php?id=<?= $commande->id ?>">Annuler</a></td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</body>
</html>
