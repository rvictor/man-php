<?php
session_start();
include "gestionBD.inc.php";

function ajoutPanier($modele, $quantite, $prix)
{
    if(!isset($_SESSION["panier"])){
        $_SESSION["panier"] = [];
    }
    if(isset($_SESSION['panier'][$modele])){
        $_SESSION["panier"][$modele]["quantite"] += $quantite;
        $_SESSION["panier"][$modele]["prix"] += $prix;
    }
    else{
        $_SESSION["panier"][$modele] = ["quantite" => $quantite, "prix" => $prix];
    }
}

if(isset($_SESSION)){
    if(empty($_SESSION["login"])){
        header("Location: login.php");
    }
}
if(isset($_GET)) {
    if(isset($_GET["panier"])){
        if(!empty($_GET["panier"])){
            $verif = $_GET["panier"];
            if($verif == "ok"){

                $pdo = connexionBd();
                $id_client = $_SESSION["login"]->id;
                $sql = "INSERT INTO man_commande (quantite,type,id_client) VALUES (:quantite, :type, :id_client)";
                $insert = $pdo->prepare($sql);
                foreach ($_SESSION["panier"] as $modele => $details) {
                    $nbTS = $details["quantite"];
                    $insert->bindValue(":quantite", $nbTS);
                    $insert->bindValue(":type", $modele);
                    $insert->bindValue(":id_client", $id_client);
                    $insert->execute();

                }
                unset($_SESSION["panier"]);
                header("Location: voirCommandes.php");


            }
        }
    }
    if (!empty($_GET['nbTS']) && !empty($_GET['modele'])) {
        $nbTS = htmlspecialchars($_GET['nbTS']);
        if ($nbTS < 0 && is_string($nbTS)) {
            $error = "Quantité erronée";
        }

        $modele = htmlspecialchars($_GET['modele']);
        $prix = $nbTS * 15;

        ajoutPanier($modele, $nbTS, $prix);

        if ($prix >= 50) {
            $total = $prix + 4;
        }
        $total = $prix + 8;


    } /*else {
        $error = "Vous n'avez pas encore passé de commande";
        $errorSaisie = "Vous n'avez pas saisie de quantité !";
        header('Location: index.php?messageError=' . $errorSaisie);
    }*/
}

?>
<?php include('elements/head.inc.php') ; ?>
   <body>
    <header>
        <h1>Initiation à la programmation</h1>
      <?php       
            include('elements/menu.inc.php') ;
        ?>
    </header>
    <section id="corps"> 
        <header><h2>Exercice 2</h2></header>
        <main>
            <article>
                <?php if(!isset($error)) : ?>
                <h3>Coût de votre commande : <?= $total ?> euros (prix + frais de port) - Modèle : <?= $modele ?> </h3>
                <?php else : ?>
                <h3><?= $error?> </h3>
                <?php endif; ?>
                <p>
                    <a class="btn" href="index.php?modifier=ok">Modifier la commande</a>
                </p>

            </article>
        </main>
        <aside>

        </aside>
    </section>
    
     <?php include('elements/footer.inc.php') ; ?>
