<?php
include('elements/head.inc.php');
session_start();
if(isset($_GET)){
    if(!empty($_GET["messageError"])){
        $messageError = htmlspecialchars($_GET["messageError"]);
    }
    if(!empty($_GET["error"])){
        $error = htmlspecialchars($_GET["error"]);
    }
}

?>
<body>
    <header>
        <a name="top"></a>
        <h1>Initiation à la programmation</h1>
        <?php  include('elements/menu.inc.php') ; ?>
    </header> 
    <main id="corps">
        <section>
            <h2>Exercice 1</h2>
            <article>
                <?php if(isset($messageError)) : ?>
                <p><?= $messageError ?></p>
                <?php endif; ?>
                <h3>Traitement d'un formulaire</h3>
                <form method="get" action="traiterCommande.php" id="commandeTS"">

                    <label for="saisieNbTS">Nombre de t-shirts :</label>
                    <input type="number" name="nbTS"  id="saisieNbTS" class="validate"/>

                    <input type="radio" name="modele" value="F" id="femme"  class="with-gap" checked/>
                    <label for="femme">Femme</label>

                    <input type="radio" name="modele" value="H"  class="with-gap" id="homme"/>
                    <label for="homme">Homme</label>

                    <input type="submit" name="commande" class="btn" value="Commander"/>
                </form>            
            </article>    
            <article>
                <h3>Commande rapide</h3>
                <p><a class="btn rapide" href="traiterCommande.php?nbTS=1&modele=F">Commander un t-shirt féminin immédiatement !</a></p>
            </article>
        </section>
        <section>
            <h2>Exercice 2</h2>
            <article>
                <?php if(!isset($_SESSION["login"]) && empty($_SESSION["login"])) : ?>
                <h3>Inscription en tant que client</h3>
                <?php if(isset($error)) : ?>
                <p><?= $error ?></p>
                <?php endif; ?>

                <form method="post" action="inscription.php" enctype="multipart/form-data">
                    <label for="nomClient">Votre nom :</label>
                    <input type="text" name="nomClient" id="nomClient" class="validate"/>

                    <label for="prenomClient">Votre prénom :</label>
                    <input type="text" name="prenomClient" id="prenomClient" class="validate"/>

                    <label for="adresseClient">Votre adresse :</label>
                    <input type="text" name="adresseClient" id="adresseClient" class="validate"/>

                    <label for="cpClient">Votre code postal :</label>
                    <input type="number" name="cpClient" id="cpClient" class="validate"/>

                    <label for="villeClient">Votre ville :</label>
                    <input type="text" name="villeClient" id="villeClient" class="validate"/>

                    <label for="emailClient">Votre email :</label>
                    <input type="email" name="emailClient" id="emailClient" class="validate"/>

                    <label for="mdpClient">Votre mot de passe :</label>
                    <input type="password" name="mdpClient" id="mdpClient" class="validate"/>

                    <label for="mdpClient2">Confirmez votre mot de passe :</label>
                    <input type="password" name="mdpClient2" id="mdpClient2" class="validate"/>

                    <h3>Tendances vestimentaires</h3>
                    <p>
                        <input type="checkbox"  id="coton" name="tendances[]" value="Coton">
                        <label for="coton">Coton</label>
                    </p>

                    <p>
                        <input type="checkbox"  id="lycra" name="tendances[]" value="Lycra">
                        <label for="lycra">Lycra</label>
                    </p>

                    <p>
                            <input type="checkbox" id="mancheL" name="tendances[]"  value="Manche Longue" />
                            <label for="mancheL">Manche longue</label>
                    </p>

                    <p>
                            <input type="checkbox" id="mancheC" name="tendances[]" value="Manche Courte" />
                            <label for="mancheC">Manche courte</label>
                    </p>

                    <h3>Photo de profil</h3>

                    <label for="photo">Votre photo de profil :</label>
                    <input type="file" name="photo" id="photo" accept="image/jpeg" class="validate"/>

                    <input type="submit" class="btn"  name="sendInscription" value="S'inscrire"/>
                </form>
                <?php else : ?>
                <p>Bonjour <?= $_SESSION["login"]->prenom?> <?= $_SESSION["login"]->nom?> </p>
                <?php endif; ?>
            </article>
        </section>
    </main>
    <aside>
    </aside>
    <?php include('elements/footer.inc.php') ; ?>
