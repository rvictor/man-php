<?php

session_start();
include "gestionBD.inc.php";
if(isset($_SESSION)){
    if(empty($_SESSION["login"])){
        header("Location: login.php");
    }
}

if(isset($_GET)){
    if(!empty($_GET['id'])){
        $id = htmlspecialchars($_GET['id']);
        $pdo = connexionBd();
        $sql = "DELETE FROM man_commande WHERE id = :id";
        $delete = $pdo->prepare($sql);
        $delete->bindValue(":id", $id);
        $delete->execute();
        header("Location: voirCommandes.php");
    }
}

?>