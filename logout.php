<?php
session_start();
if(isset($_SESSION["login"])) {
    session_destroy();
    unset($_SESSION["login"]);
    header("Location: index.php");
}
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Déconnexion</title>S
</head>
<body>
    <p>Vous êtes déconnecté</p>
    <a href="index.php">Retour à l'accueil</a>
</body>
</html>
