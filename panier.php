<?php
session_start();

if(isset($_SESSION)){
    if(empty($_SESSION["login"])){
        header("Location: login.php");
    }
}
$prixTotal = 0;
if(!isset($_SESSION["panier"])){
    $panierError = "Votre panier est vide";
}

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/panier.css">
    <title>Votre panier</title>
</head>
<body>
    <?php if(!isset($panierError)) : ?>
    <h1>Votre panier</h1>
    <table>
        <thead>
            <tr>
                <th>Modèle</th>
                <th>Quantité</th>
                <th>Prix</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($_SESSION["panier"] as $modele => $details) : ?>
            <tr>
                <td><?= $modele ?></td>
                <td><?= $details["quantite"] ?></td>
                <td><?= $details["prix"] ?> €</td>
            </tr>
            <?php $prixTotal += $details["prix"]; ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="2">Total</td>
                <td><?= $prixTotal ?> €</td>
            </tr>
        </tbody>
    </table>
    <a href="traiterCommande.php?panier=ok">Valider le panier</a>
    <a href="index.php">Retour à l'accueil</a>
    <?php else : ?>
    <h3><?= $panierError?> </h3>
    <?php endif; ?>
</body>
</html>
