<?php

include "gestionBD.inc.php";

if(isset($_POST["sendInscription"])){
    if(!empty($_POST["nomClient"])
        && !empty($_POST["prenomClient"])
        && !empty($_POST["adresseClient"])
        && !empty($_POST["cpClient"])
        && !empty($_POST["villeClient"])
        && !empty($_POST["emailClient"])
        && !empty($_POST["mdpClient"])
        && !empty($_POST["mdpClient2"]))
    {
        if($_POST["mdpClient"] != $_POST["mdpClient2"]){
            $error = "Les mots de passe ne correspondent pas";
            header("Location: index.php?error=$error");
        }

        $pdo = connexionBd();
        // Ajoute dans la bd

        $nom = htmlspecialchars($_POST["nomClient"]);
        $prenom = htmlspecialchars($_POST["prenomClient"]);
        $adresse = htmlspecialchars($_POST["adresseClient"]);
        $cp = htmlspecialchars($_POST["cpClient"]);
        $ville = htmlspecialchars($_POST["villeClient"]);
        $email = htmlspecialchars($_POST["emailClient"]);
        $mdp = htmlspecialchars($_POST["mdpClient"]);
        $mdp2 = htmlspecialchars($_POST["mdpClient2"]);

        $sql = "INSERT INTO man_client (nom, prenom, email,mot_de_passe, adresse, code_postal, ville) VALUES (:nom, :prenom, :email, :mdp, :adresse, :cp, :ville)";
        $insert = $pdo->prepare($sql);
        $insert->bindValue(":nom", $nom);
        $insert->bindValue(":prenom", $prenom);
        $insert->bindValue(":adresse", $adresse);
        $insert->bindValue(":cp", $cp);
        $insert->bindValue(":ville", $ville);
        $insert->bindValue(":email", $email);
        $insert->bindValue(":mdp", password_hash($mdp, PASSWORD_BCRYPT));
        $insert->execute();



        if(!empty($_FILES)){
            $photo = $_FILES["photo"]["name"];
            $files = $_FILES["photo"]["tmp_name"];
            move_uploaded_file($files, "photo/".$photo);
        }

    } else{
        $error = "Veuillez remplir tous les champs";
        header("Location: index.php?error=$error");
    }
}

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/register.css">
    <title>Inscription</title>
</head>
<body>

    <?php if(!isset($error)) : ?>
    <p>Bonjour <?= $prenom ?> <?=$nom?> </p>
    <p>Votre adresse est : <?= $adresse ?> <?= $cp ?> <?= $ville ?></p>
    <p>Votre email est : <?= $email ?></p>
    <h1>Vos tendances vestimentaires</h1>
    <ul>
        <?php foreach($_POST["tendances"] as $tendance) : ?>
            <li><?= $tendance ?></li>
        <?php endforeach; ?>
    <?php endif; ?>
    </ul>

    <p>Votre photo de profil :</p>
    <?php if(!empty($_FILES)) : ?>
    <img src="photo/<?= $photo ?>" alt="photo de profil">
    <?php else : ?>
    <p>Vous n'avez pas mis de photo de profil</p>
    <?php endif; ?>


</body>
</html>