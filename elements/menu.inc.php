<nav>
        <ul> 
            <li><a  href="index.php">Accueil</a></li>
            <li><a  href="traiterCommande.php">Commande</a></li>
            <?php if(isset($_SESSION["login"]) && !empty($_SESSION["login"])) : ?>
                <li><a  href="voirCommandes.php">Mes commandes</a></li>
                <li><a  href="panier.php">Panier</a></li>
                <li><a  href="logout.php">Déconnexion</a></li>
            <?php else : ?>
                <li><a  href="login.php">Connexion</a></li>
            <?php endif; ?>
        </ul>
    </nav>